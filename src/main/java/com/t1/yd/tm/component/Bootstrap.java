package com.t1.yd.tm.component;

import com.t1.yd.tm.api.repository.ICommandRepository;
import com.t1.yd.tm.api.repository.IProjectRepository;
import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.api.repository.IUserRepository;
import com.t1.yd.tm.api.service.*;
import com.t1.yd.tm.command.AbstractCommand;
import com.t1.yd.tm.command.data.AbstractDataCommand;
import com.t1.yd.tm.command.data.DataBase64LoadCommand;
import com.t1.yd.tm.command.data.DataBinaryLoadCommand;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.exception.system.ArgumentNotSupportedException;
import com.t1.yd.tm.exception.system.CommandNotSupportedException;
import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.model.Task;
import com.t1.yd.tm.repository.CommandRepository;
import com.t1.yd.tm.repository.ProjectRepository;
import com.t1.yd.tm.repository.TaskRepository;
import com.t1.yd.tm.repository.UserRepository;
import com.t1.yd.tm.service.*;
import com.t1.yd.tm.util.SystemUtil;
import com.t1.yd.tm.util.TerminalUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "com.t1.yd.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();


    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> commandClasses = reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : commandClasses) registry(clazz);
    }

    public void run(String[] args) {
        initDemoData();
        initLogger();
        initPID();
        initData();

        processArguments(args);

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command, true);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                e.getMessage();
                System.err.println("[FAIL]");
                loggerService.error(e);
            }
        }
    }

    private void initData() {
        final boolean checkBinary = Files.exists(Paths.get(AbstractDataCommand.FILE_BINARY));
        if (checkBinary) processCommand(DataBinaryLoadCommand.NAME, false);
        if (checkBinary) return;
        final boolean checkBase64 = Files.exists(Paths.get(AbstractDataCommand.FILE_BASE64));
        if (checkBase64) processCommand(DataBase64LoadCommand.NAME, false);
    }

    private void initDemoData() {
        userService.add(userService.create("admin", "admin", "admin@mail.ru", Role.ADMIN));
        userService.add(userService.create("user1", "user1", "user1@mail.ru"));
        userService.add(userService.create("user2", "user2", "user2@mail.ru"));

        projectRepository.add(userService.findByLogin("user1").getId(), new Project("project1", "my first project"));
        projectRepository.add(userService.findByLogin("user1").getId(), new Project("project2", "my 2nd project"));
        projectRepository.add(userService.findByLogin("user1").getId(), new Project("project3", "my 3rd project"));
        projectRepository.add(userService.findByLogin("user2").getId(), new Project("project4", "my 4th project"));
        projectRepository.add(userService.findByLogin("user2").getId(), new Project("project5", "my 5th project"));

        taskRepository.add(userService.findByLogin("user1").getId(), new Task("task1", "my 1st task"));
        taskRepository.add(userService.findByLogin("user1").getId(), new Task("task2", "my 2nd task"));
        taskRepository.add(userService.findByLogin("user2").getId(), new Task("task3", "my 3rd task"));
        taskRepository.add(userService.findByLogin("user2").getId(), new Task("task4", "my 4th task"));

        projectTaskService.bindTaskToProject(userService.findByLogin("user1").getId(), taskRepository.findOneByIndex(0).getId(), projectRepository.findOneByIndex(0).getId());
        projectTaskService.bindTaskToProject(userService.findByLogin("user1").getId(), taskRepository.findOneByIndex(1).getId(), projectRepository.findOneByIndex(0).getId());
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        final File file = new File(filename);
        file.deleteOnExit();
    }

    private void processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        if (args[0] == null) return;
        processArgument(args[0]);
    }

    private void processArgument(@NotNull final String arg) {
        @Nullable final AbstractCommand command = commandService.getCommandByArgument(arg);
        if (command == null) throw new ArgumentNotSupportedException();
        command.execute();
    }

    private void processCommand(@NotNull final String commandName, final boolean checkRoles) {
        @Nullable final AbstractCommand command = commandService.getCommandByName(commandName);
        if (command == null) throw new CommandNotSupportedException();
        if (checkRoles) authService.checkRoles(command.getRoles());
        command.execute();
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> loggerService.info("** TASK MANAGER IS SHUTTING DOWN **")));
    }

    private void exit() {
        System.exit(0);
    }

}