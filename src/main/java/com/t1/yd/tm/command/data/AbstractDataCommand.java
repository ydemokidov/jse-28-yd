package com.t1.yd.tm.command.data;

import com.t1.yd.tm.command.AbstractCommand;
import com.t1.yd.tm.dto.Domain;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    @NotNull
    public static final String FILE_XML = "./data.xml";

    @NotNull
    public static final String FILE_JSON = "./data.json";

    @NotNull
    public static final String FILE_YAML = "./data.yaml";

    @NotNull
    public final String CONTEXT_FACTORY = "java.sml.bind.context.factory";

    @NotNull
    public final String CONTEXT_FACTORY_JAXB = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    public final String MEDIA_TYPE = "eclipselink.media-type";

    @NotNull
    public final String APPLICATION_TYPE_JSON = "application/json";

    @NotNull
    public Domain getDomain() {
        @NotNull Domain domain = new Domain();
        domain.setUsers(serviceLocator.getUserService().findAll());
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        return domain;
    }

    public void setDomain(@Nullable Domain domain) {
        if (domain == null) return;
        serviceLocator.getUserService().add(domain.getUsers());
        serviceLocator.getProjectService().add(domain.getProjects());
        serviceLocator.getTaskService().add(domain.getTasks());
        serviceLocator.getAuthService().logout();
    }

}
