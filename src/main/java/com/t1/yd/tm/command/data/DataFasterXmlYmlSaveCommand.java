package com.t1.yd.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import com.t1.yd.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DataFasterXmlYmlSaveCommand extends AbstractFasterXmlSaveCommand {

    @NotNull
    private final String name = "save_fasterxml_yml";

    @NotNull
    private final String description = "Save YAML with FasterXml library";

    @Override
    @NotNull
    protected ObjectMapper getMapper() {
        return new YAMLMapper();
    }

    @Override
    @NotNull
    protected String getFile() {
        return FILE_YAML;
    }

    @Override
    @NotNull
    public String getName() {
        return name;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @NotNull
    public String getDescription() {
        return description;
    }

}
