package com.t1.yd.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.t1.yd.tm.dto.Domain;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Files;
import java.nio.file.Paths;

public abstract class AbstractFasterXmlLoadCommand extends AbstractDataCommand {

    @NotNull
    protected abstract ObjectMapper getMapper();

    @NotNull
    protected abstract String getFile();

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD FASTERXML XML]");

        @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(getFile()));
        @NotNull final String dataStr = new String(bytes);
        @NotNull final ObjectMapper mapper = getMapper();
        @NotNull final Domain domain = mapper.readValue(dataStr, Domain.class);
        setDomain(domain);

        System.out.println("[DATA LOADED]");
    }

}
