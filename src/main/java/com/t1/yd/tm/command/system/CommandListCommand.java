package com.t1.yd.tm.command.system;

import com.t1.yd.tm.api.model.ICommand;
import com.t1.yd.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

public class CommandListCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "commands";

    @NotNull
    public static final String ARGUMENT = "-comm";

    @NotNull
    public static final String DESCRIPTION = "Show available commands";

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");

        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();

        for (@NotNull final ICommand command : commands) {
            @NotNull final String name = command.getName();
            if (name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
