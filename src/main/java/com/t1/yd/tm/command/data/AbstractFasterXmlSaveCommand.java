package com.t1.yd.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.t1.yd.tm.dto.Domain;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

public abstract class AbstractFasterXmlSaveCommand extends AbstractDataCommand {

    @NotNull
    protected abstract ObjectMapper getMapper();

    @NotNull
    protected abstract String getFile();

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE FASTERXML]");

        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(getFile());
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectMapper mapper = getMapper();
        @NotNull final String xml = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();

        System.out.println("[DATA SAVED]");
    }

}
