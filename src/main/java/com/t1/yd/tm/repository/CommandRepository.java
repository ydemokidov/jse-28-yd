package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.repository.ICommandRepository;
import com.t1.yd.tm.command.AbstractCommand;
import com.t1.yd.tm.exception.field.NameEmptyException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> commandsByArgument = new TreeMap<>();

    @NotNull
    private final Map<String, AbstractCommand> commandsByName = new TreeMap<>();

    @Override
    public void add(@NotNull final AbstractCommand command) {
        @NotNull final String name = command.getName();
        if (name.isEmpty()) throw new NameEmptyException();
        commandsByName.put(name, command);
        @Nullable final String argument = command.getArgument();
        if (argument == null || argument.isEmpty()) return;
        commandsByArgument.put(argument, command);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArgument(@NotNull final String argument) {
        return commandsByArgument.get(argument);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return commandsByName.get(name);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return commandsByName.values();
    }

}
