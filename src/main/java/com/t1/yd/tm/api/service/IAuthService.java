package com.t1.yd.tm.api.service;

import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IAuthService {

    void checkRoles(@NotNull Role[] roles);

    @NotNull
    User registry(@NotNull String login, @NotNull String password, @NotNull String email);

    void login(@NotNull String login, @NotNull String password);

    void logout();

    boolean isAuth();

    @NotNull
    String getUserId();

    @Nullable
    User getUser();

}
